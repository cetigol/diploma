package com.autogara.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.autogara.spring.model.Office;
import com.autogara.spring.service.OfficeService;

@ManagedBean(name = "indexMB")
@ViewScoped
public class IndexManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{OfficeService}")
	private OfficeService officeService;
	private ResourceBundle messageBundle;

	private MapModel simpleModel;
	private Office selectedOffice;
	private Marker selectedMarker;

	private String name;
	private String locality;
	private String address;
	private String phone;

	@PostConstruct
	public void init() {
		messageBundle = ResourceBundle.getBundle("messages/messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());
		offices = officeService.getAll();
		getMarkers();
	}

	public void getMarkers() {
		messageBundle = ResourceBundle.getBundle("messages/messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());
		simpleModel = new DefaultMapModel();
		for (Office office : offices) {
			LatLng coord1 = new LatLng(Double.valueOf(office.getLocality().getLatitude()),
					Double.valueOf(office.getLocality().getLongitude()));
			simpleModel.addOverlay(
					new Marker(coord1, messageBundle.getString(office.getName()), getDataForMarker(office)));
		}
	}

	public void onMarkerSelect(OverlaySelectEvent event) {
		selectedMarker = (Marker) event.getOverlay();
	}

	public void onRowSelect(SelectEvent event) {
		Office office = (Office) event.getObject();
		simpleModel = new DefaultMapModel();
		LatLng coord1 = new LatLng(Double.valueOf(office.getLocality().getLatitude()),
				Double.valueOf(office.getLocality().getLongitude()));
		simpleModel.addOverlay(new Marker(coord1, messageBundle.getString(office.getName()), getDataForMarker(office)));
	}

	private String getDataForMarker(Office office) {
		StringBuilder build = new StringBuilder();
		build.append(messageBundle.getString("info.popup.name"));
		build.append(":");
		build.append(messageBundle.getString(office.getName()));
		build.append("\n");
		build.append(messageBundle.getString("info.popup.locality"));
		build.append(":");
		build.append(messageBundle.getString(office.getLocality().getName()));
		build.append("\n");
		build.append(messageBundle.getString("info.popup.address"));
		build.append(":");
		build.append(office.getAddress());
		build.append("\n");
		build.append(messageBundle.getString("info.popup.phone"));
		build.append(":");
		build.append(office.getPhone());
		return build.toString();
	}

	public void redirectIndex() throws IOException {
		String contextPath = FacesContext.getCurrentInstance().getExternalContext().getContextName();
		FacesContext.getCurrentInstance().getExternalContext().redirect("/" + contextPath);
	}

	public void logout() throws IOException {
		String contextPath = FacesContext.getCurrentInstance().getExternalContext().getContextName();
		FacesContext.getCurrentInstance().getExternalContext().redirect("/" + contextPath + "/logout");
	}

	private List<Office> offices;

	public List<Office> getOffices() {
		return offices;
	}

	public void setOffices(List<Office> offices) {
		this.offices = offices;
	}

	public OfficeService getOfficeService() {
		return officeService;
	}

	public void setOfficeService(OfficeService officeService) {
		this.officeService = officeService;
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}

	public Office getSelectedOffice() {
		return selectedOffice;
	}

	public void setSelectedOffice(Office selectedOffice) {
		this.selectedOffice = selectedOffice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Marker getSelectedMarker() {
		return selectedMarker;
	}

	public void setSelectedMarker(Marker selectedMarker) {
		this.selectedMarker = selectedMarker;
	}

}
