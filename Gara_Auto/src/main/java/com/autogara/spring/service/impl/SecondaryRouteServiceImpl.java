package com.autogara.spring.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.SecondaryRouteDAO;
import com.autogara.spring.model.SecondaryRoute;
import com.autogara.spring.service.SecondaryRouteService;

@Service("secondaryRouteService")
public class SecondaryRouteServiceImpl implements SecondaryRouteService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private SecondaryRouteDAO secondaryRouteDAO;

	@Override
	@Transactional(readOnly = true)
	public List<SecondaryRoute> getAll() {
		return secondaryRouteDAO.getAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<SecondaryRoute> getAllWithoutRoute() {
		return secondaryRouteDAO.getAllWithoutRoute();
	}

	@Override
	@Transactional
	public void saveOrUpdate(SecondaryRoute secondaryRoute) {
		secondaryRouteDAO.save(secondaryRoute);
	}

}
