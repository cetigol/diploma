package com.autogara.spring.service;

import java.util.List;

import com.autogara.spring.model.Gallery;

public interface GalleryService {

	List<Gallery> getAll();

	void insert(Gallery gallery);
}
