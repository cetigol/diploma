package com.autogara.controller.route;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.autogara.spring.model.Bus;
import com.autogara.spring.model.Locality;
import com.autogara.spring.model.Office;
import com.autogara.spring.model.Route;
import com.autogara.spring.model.SecondaryRoute;
import com.autogara.spring.model.enums.LocalityStatus;
import com.autogara.spring.service.BusService;
import com.autogara.spring.service.LocalityService;
import com.autogara.spring.service.OfficeService;
import com.autogara.spring.service.RouteService;
import com.autogara.spring.service.SecondaryRouteService;
import com.autogara.spring.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ManagedBean(name = "routeFormMB")
@ViewScoped
public class RouteFormManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String ICON_START_LOCALITY = "http://maps.gstatic.com/mapfiles/markers2/marker_greenA.png";
	private static final String ICON_DESTINATION_LOCALITY = "http://www.google.com/mapfiles/markerB.png";
	private static final String ICON_WAY_POINT = "http://www.google.com/mapfiles/marker_yellow.png";

	private MapModel simpleModel;
	private Route selectedRoute;
	private Office selectedOffice;
	private Bus selectedBus;

	private String timeHour;
	private String timeMin;

	private String timeTravelHour;
	private String timeTravelMin;

	private List<Locality> allLocality;
	private List<Locality> allStartLocality;
	private List<Office> allOffice;
	private List<Bus> allBus;
	private List<Long> selectedDays;
	private DualListModel<SecondaryRoute> secondaryRoutes;

	private List<SecondaryRoute> secondaryRouteList;

	@ManagedProperty(value = "#{routeService}")
	private RouteService routeService;
	@ManagedProperty(value = "#{localityService}")
	private LocalityService localityService;
	@ManagedProperty(value = "#{OfficeService}")
	private OfficeService officeService;
	@ManagedProperty(value = "#{busService}")
	private BusService busService;
	@ManagedProperty(value = "#{userService}")
	private UserService userService;
	@ManagedProperty(value = "#{secondaryRouteService}")
	private SecondaryRouteService secondaryRouteService;

	private ResourceBundle messageBundle;

	@PostConstruct
	public void init() {
		selectedRoute = new Route();
		selectedOffice = new Office();
		selectedRoute.setStartLocality(new Locality());
		selectedRoute.setDestinationLocality(new Locality());
		selectedBus = new Bus();

		timeHour = "0";
		timeMin = "00";
		timeTravelHour = "0";
		timeTravelMin = "00";

		allLocality = new ArrayList<Locality>();
		allStartLocality = new ArrayList<Locality>();
		allOffice = new ArrayList<Office>();
		allBus = new ArrayList<Bus>();
		selectedDays = new ArrayList<Long>();
		List<SecondaryRoute> seconddaryRoutesSource = new ArrayList<SecondaryRoute>();
		List<SecondaryRoute> seconddaryRoutesTarget = new ArrayList<SecondaryRoute>();
		selectedRoute.setSecondaryRoutes(seconddaryRoutesTarget);

		allLocality.addAll(localityService.getAll());
		allOffice.addAll(officeService.getAll());
		allStartLocality.addAll(allLocality);
		allBus.addAll(busService.getAll());
		secondaryRouteList = secondaryRouteService.getAllWithoutRoute();
		seconddaryRoutesSource.addAll(secondaryRouteList);
		secondaryRoutes = new DualListModel<SecondaryRoute>(seconddaryRoutesSource, seconddaryRoutesTarget);
	}

	public void busStationChange(AjaxBehaviorEvent event) {
		Integer officeId = (Integer) event.getComponent().getAttributes().get("value");
		if (officeId != null) {
			selectedOffice = officeService.getById(officeId);
			selectedRoute.setStartLocality(selectedOffice.getLocality());
			selectedRoute.setOffice(selectedOffice);
			allStartLocality = new ArrayList<Locality>();
			allStartLocality.add(selectedOffice.getLocality());
			drawMap();
		} else {
			allStartLocality = new ArrayList<Locality>();
			selectedRoute.setStartLocality(new Locality());
			selectedRoute.setOffice(new Office());
			allStartLocality.addAll(allLocality);
		}
	}

	public void localityChange(AjaxBehaviorEvent event) {
		if (selectedRoute.getDestinationLocality().getIdLocality() != null) {
			selectedRoute.setDestinationLocality(
					localityService.getById(selectedRoute.getDestinationLocality().getIdLocality()));
		} else {
			selectedRoute.setDestinationLocality(new Locality());
		}
		if (selectedRoute.getStartLocality().getIdLocality() == null) {
			selectedRoute.setStartLocality(new Locality());
		}
		if (selectedRoute.getStartLocality().getIdLocality() != null
				&& selectedRoute.getDestinationLocality().getIdLocality() != null) {
			drawMap();
		}
	}

	public void saveRoute() {
		selectedRoute.setBus(selectedBus);
		selectedRoute.setOffice(selectedOffice);
		selectedRoute.setTimeStart(timeHour + ":" + timeMin);
		selectedRoute.setTimeTravel(timeTravelHour + ":" + timeTravelMin);

		if (!secondaryRoutes.getTarget().isEmpty()) {
			selectedRoute.setSecondaryRoutes(secondaryRoutes.getTarget());
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		selectedRoute.setUser(userService.getUserByLogin(currentPrincipalName));
		routeService.save(selectedRoute);
	}

	public void editRoute(Integer routeId) {
		selectedRoute = routeService.getById(routeId);
		selectedBus = selectedRoute.getBus();
		selectedOffice = selectedRoute.getOffice();
		String[] time = selectedRoute.getTimeStart().split(":");
		timeHour = time[0];
		timeMin = time.length == 2 ? time[1] : "00";
		String[] timeTravel = selectedRoute.getTimeTravel().split(":");
		timeTravelHour = timeTravel[0];
		timeTravelMin = timeTravel.length == 2 ? timeTravel[1] : "00";
		secondaryRoutes.setTarget(selectedRoute.getSecondaryRoutes());
		drawMap();
	}

	public void onTransfer(TransferEvent event) {
		List<SecondaryRoute> secondaryR = new ArrayList<SecondaryRoute>();
		for (SecondaryRoute secR : secondaryRoutes.getTarget()) {
			for (SecondaryRoute secR2 : secondaryRouteList) {
				if (secR.getIdSecondaryRoute() == secR2.getIdSecondaryRoute()) {
					secondaryR.add(secR2);
					break;
				}
			}
		}

		selectedRoute.setSecondaryRoutes(secondaryR);
		drawMap();
	}

	private void drawMap() {
		if (selectedRoute.getStartLocality() != null && selectedRoute.getDestinationLocality() != null
				&& selectedRoute.getStartLocality().getIdLocality() != null
				&& selectedRoute.getDestinationLocality().getIdLocality() != null) {
			RequestContext context = RequestContext.getCurrentInstance();
			List<Marker> markers = new ArrayList<Marker>();

			messageBundle = ResourceBundle.getBundle("messages/messages",
					FacesContext.getCurrentInstance().getViewRoot().getLocale());

			markers.add(setMarkerInfoFromLocality(selectedRoute.getStartLocality(), LocalityStatus.START_LOCALITY, ""));
			markers.add(setMarkerInfoFromLocality(selectedRoute.getDestinationLocality(),
					LocalityStatus.DESTINATION_LOCALITY, ""));

			for (SecondaryRoute route : selectedRoute.getSecondaryRoutes()) {
				markers.add(setMarkerInfoFromLocality(route.getLocality(), LocalityStatus.WAY_POINT, ""));
			}

			ObjectMapper mapper = new ObjectMapper();

			try {
				context.execute("calculateAndDisplayRoute(" + mapper.writeValueAsString(markers) + ",'gMapForm')");
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
	}

	private Marker setMarkerInfoFromLocality(Locality locality, LocalityStatus status, String message) {
		String iconUrl;

		switch (status) {
		case START_LOCALITY:
			iconUrl = ICON_START_LOCALITY;
			break;
		case DESTINATION_LOCALITY:
			iconUrl = ICON_DESTINATION_LOCALITY;
			break;
		case WAY_POINT:
			iconUrl = ICON_WAY_POINT;
			break;
		default:
			iconUrl = "";
			break;
		}
		Marker marker = new Marker(
				new LatLng(Double.valueOf(locality.getLatitude()), Double.valueOf(locality.getLongitude())),
				messageBundle.getString(locality.getName()), message, iconUrl);
		return marker;
	}

	public Route getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(Route selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}

	public List<Locality> getAllLocality() {
		return allLocality;
	}

	public void setAllLocality(List<Locality> allLocality) {
		this.allLocality = allLocality;
	}

	public RouteService getRouteService() {
		return routeService;
	}

	public void setRouteService(RouteService routeService) {
		this.routeService = routeService;
	}

	public LocalityService getLocalityService() {
		return localityService;
	}

	public void setLocalityService(LocalityService localityService) {
		this.localityService = localityService;
	}

	public List<Office> getAllOffice() {
		return allOffice;
	}

	public void setAllOffice(List<Office> allOffice) {
		this.allOffice = allOffice;
	}

	public OfficeService getOfficeService() {
		return officeService;
	}

	public void setOfficeService(OfficeService officeService) {
		this.officeService = officeService;
	}

	public Office getSelectedOffice() {
		return selectedOffice;
	}

	public void setSelectedOffice(Office selectedOffice) {
		this.selectedOffice = selectedOffice;
	}

	public List<Locality> getAllStartLocality() {
		return allStartLocality;
	}

	public void setAllStartLocality(List<Locality> allStartLocality) {
		this.allStartLocality = allStartLocality;
	}

	public Bus getSelectedBus() {
		return selectedBus;
	}

	public void setSelectedBus(Bus selectedBus) {
		this.selectedBus = selectedBus;
	}

	public List<Bus> getAllBus() {
		return allBus;
	}

	public void setAllBus(List<Bus> allBus) {
		this.allBus = allBus;
	}

	public BusService getBusService() {
		return busService;
	}

	public void setBusService(BusService busService) {
		this.busService = busService;
	}

	public ResourceBundle getMessageBundle() {
		return messageBundle;
	}

	public void setMessageBundle(ResourceBundle messageBundle) {
		this.messageBundle = messageBundle;
	}

	public String getTimeHour() {
		return timeHour;
	}

	public void setTimeHour(String timeHour) {
		this.timeHour = timeHour;
	}

	public String getTimeMin() {
		return timeMin;
	}

	public void setTimeMin(String timeMin) {
		this.timeMin = timeMin;
	}

	public String getTimeTravelHour() {
		return timeTravelHour;
	}

	public void setTimeTravelHour(String timeTravelHour) {
		this.timeTravelHour = timeTravelHour;
	}

	public String getTimeTravelMin() {
		return timeTravelMin;
	}

	public void setTimeTravelMin(String timeTravelMin) {
		this.timeTravelMin = timeTravelMin;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public DualListModel<SecondaryRoute> getSecondaryRoutes() {
		return secondaryRoutes;
	}

	public void setSecondaryRoutes(DualListModel<SecondaryRoute> secondaryRoutes) {
		this.secondaryRoutes = secondaryRoutes;
	}

	public SecondaryRouteService getSecondaryRouteService() {
		return secondaryRouteService;
	}

	public void setSecondaryRouteService(SecondaryRouteService secondaryRouteService) {
		this.secondaryRouteService = secondaryRouteService;
	}

	public List<Long> getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(List<Long> selectedDays) {
		this.selectedDays = selectedDays;
	}

}
