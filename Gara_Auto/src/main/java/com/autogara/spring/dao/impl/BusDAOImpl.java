package com.autogara.spring.dao.impl;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.BusDAO;
import com.autogara.spring.model.Bus;

@Repository
public class BusDAOImpl extends GenericDAOImpl<Bus, Long> implements BusDAO, Serializable {

	private static final long serialVersionUID = 1L;

}
