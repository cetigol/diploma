package com.autogara.controller.route;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.autogara.spring.model.Locality;
import com.autogara.spring.model.Route;
import com.autogara.spring.model.SecondaryRoute;
import com.autogara.spring.model.enums.LocalityStatus;
import com.autogara.spring.service.LocalityService;
import com.autogara.spring.service.OfficeService;
import com.autogara.spring.service.RouteService;
import com.autogara.spring.service.impl.LazyRouteDataModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ManagedBean(name = "routeMB")
@ViewScoped
public class RouteManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String ICON_START_LOCALITY = "http://maps.gstatic.com/mapfiles/markers2/marker_greenA.png";
	private static final String ICON_DESTINATION_LOCALITY = "http://www.google.com/mapfiles/markerB.png";
	private static final String ICON_WAY_POINT = "http://www.google.com/mapfiles/marker_yellow.png";

	@ManagedProperty(value = "#{OfficeService}")
	private OfficeService officeService;
	@ManagedProperty(value = "#{routeService}")
	private RouteService routeService;
	@ManagedProperty(value = "#{localityService}")
	private LocalityService localityService;

	private ResourceBundle messageBundle;

	private LazyDataModel<Route> lazyModel;

	private Route selectedRoute;
	private Route oneMenu;
	private MapModel simpleModel;

	private List<String> startLocalitys;
	private List<String> destinationLocalitys;
	private List<String> officesName;

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}

	@PostConstruct
	public void init() {
		messageBundle = ResourceBundle.getBundle("messages/messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());
		lazyModel = new LazyRouteDataModel(routeService);
		startLocalitys = new ArrayList<String>();
		destinationLocalitys = new ArrayList<String>();
		officesName = new ArrayList<String>();

		startLocalitys.addAll(localityService.getAllStartLocalityName());
		destinationLocalitys.addAll(localityService.getAllDestinationLocalityName());
		officesName.addAll(officeService.getAllOfficeName());
	}

	public void onRowSelect(SelectEvent event) {
		messageBundle = ResourceBundle.getBundle("messages/messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());
		RequestContext context = RequestContext.getCurrentInstance();

		List<Marker> markers = new ArrayList<Marker>();

		markers.add(setMarkerInfoFromLocality(selectedRoute.getStartLocality(), LocalityStatus.START_LOCALITY, ""));
		markers.add(setMarkerInfoFromLocality(selectedRoute.getDestinationLocality(),
				LocalityStatus.DESTINATION_LOCALITY, ""));

		for (SecondaryRoute route : selectedRoute.getSecondaryRoutes()) {
			markers.add(setMarkerInfoFromLocality(route.getLocality(), LocalityStatus.WAY_POINT,
					getInfoWindowData(route.getTimeTravel(), route.getPrice(), route.getDescription())));
		}
		ObjectMapper mapper = new ObjectMapper();

		context.execute("PF('dlgRoute').show();");
		try {
			context.execute("calculateAndDisplayRoute(" + mapper.writeValueAsString(markers) + ",'gMap')");
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

	}

	public void delete(Integer idRoute) {
		routeService.delete(idRoute);
	}

	private Marker setMarkerInfoFromLocality(Locality locality, LocalityStatus status, String message) {
		String iconUrl;

		switch (status) {
		case START_LOCALITY:
			iconUrl = ICON_START_LOCALITY;
			break;
		case DESTINATION_LOCALITY:
			iconUrl = ICON_DESTINATION_LOCALITY;
			break;
		case WAY_POINT:
			iconUrl = ICON_WAY_POINT;
			break;
		default:
			iconUrl = "";
			break;
		}
		Marker marker = new Marker(
				new LatLng(Double.valueOf(locality.getLatitude()), Double.valueOf(locality.getLongitude())),
				messageBundle.getString(locality.getName()), message, iconUrl);
		return marker;
	}

	private String getInfoWindowData(String time, Double price, String description) {
		StringBuilder buildMessage = new StringBuilder();
		String NEW_LINE = "<br />";
		buildMessage.append("<b>" + messageBundle.getString("datatable.hour") + "</b>: ");
		buildMessage.append(getHour(selectedRoute.getTimeStart(), time));
		buildMessage.append(NEW_LINE);

		buildMessage.append("<b>" + messageBundle.getString("datatable.price") + "</b>: ");
		buildMessage.append(price);
		buildMessage.append(NEW_LINE);
		return buildMessage.toString();
	}

	private String getHour(String startTime, String timeTravel) {
		String[] startTimeArray = startTime.split(":");
		String[] timeTravelArray = timeTravel.split(":");

		Integer hour = Integer.parseInt(startTimeArray[0]) + Integer.parseInt(timeTravelArray[0]);
		Integer minutes = Integer.parseInt(startTimeArray[1]) + Integer.parseInt(timeTravelArray[1]);

		while (minutes > 59) {
			Integer difference = minutes - 59;
			if (difference == 1) {
				hour += 1;
				minutes = 0;
			} else {
				minutes = difference;
			}
		}

		while (hour > 23) {
			Integer difference = hour - 23;
			if (difference == 1) {
				hour = 0;
			} else {
				hour = difference - 1;
			}
		}

		StringBuilder result = new StringBuilder();

		if (hour == 0) {
			result.append("00");
		} else {
			result.append(hour);
		}
		result.append(":");
		if (minutes == 0) {
			result.append("00");
		} else if (minutes > 0 && minutes < 10) {
			result.append("0" + minutes);
		} else {
			result.append(minutes);
		}

		return result.toString();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Route getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(Route selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	public OfficeService getOfficeService() {
		return officeService;
	}

	public void setOfficeService(OfficeService officeService) {
		this.officeService = officeService;
	}

	public Route getOneMenu() {
		return oneMenu;
	}

	public void setOneMenu(Route oneMenu) {
		this.oneMenu = oneMenu;
	}

	public List<String> getStartLocalitys() {
		return startLocalitys;
	}

	public void setStartLocalitys(List<String> startLocalitys) {
		this.startLocalitys = startLocalitys;
	}

	public List<String> getDestinationLocalitys() {
		return destinationLocalitys;
	}

	public void setDestinationLocalitys(List<String> destinationLocalitys) {
		this.destinationLocalitys = destinationLocalitys;
	}

	public RouteService getRouteService() {
		return routeService;
	}

	public void setRouteService(RouteService routeService) {
		this.routeService = routeService;
	}

	public LazyDataModel<Route> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Route> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public LocalityService getLocalityService() {
		return localityService;
	}

	public void setLocalityService(LocalityService localityService) {
		this.localityService = localityService;
	}

	public List<String> getOfficesName() {
		return officesName;
	}

	public void setOfficesName(List<String> officesName) {
		this.officesName = officesName;
	}
}
