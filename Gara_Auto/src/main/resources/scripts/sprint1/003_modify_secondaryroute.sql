ALTER TABLE `autogara`.`secondaryroute` 
ADD COLUMN `time_travel` VARCHAR(45) NULL AFTER `fk_locality`,
ADD COLUMN `price` FLOAT NULL AFTER `time_travel`;
