package com.autogara.spring.dao;

import java.util.List;

public interface GenericDAO<T> {
	/**
	 * Delete the registration from database.
	 * 
	 * @param id
	 *            The ID of the registration wich we want to delete.
	 */
	public void delete(Integer id);

	/**
	 * Returns an object with data from the database by id.
	 * 
	 * @param id
	 *            The ID of the registration wich we want to select.
	 * @return Return an object the type of the model classes.
	 */
	public T getById(Integer id);

	/**
	 * Insert the data into database.
	 * 
	 * @param t
	 *            The data wich we want to insert.
	 */
	public void insert(T t);

	/**
	 * Update the registration into database by id.
	 * 
	 * @param t
	 *            The data wich we want to set.
	 */
	public void update(T t);

	public List<T> getAll();

	public void save(T t);
}
