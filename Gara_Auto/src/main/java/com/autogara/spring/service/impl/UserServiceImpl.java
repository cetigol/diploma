package com.autogara.spring.service.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.UserDAO;
import com.autogara.spring.model.User;
import com.autogara.spring.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private UserDAO userDAO;

	@Override
	@Transactional(readOnly = true)
	public User getUserByLogin(String userName) {
		return userDAO.getUserByUserName(userName);
	}

}
