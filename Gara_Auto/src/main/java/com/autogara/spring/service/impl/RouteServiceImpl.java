package com.autogara.spring.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.RouteDAO;
import com.autogara.spring.model.Route;
import com.autogara.spring.service.RouteService;

@Service("routeService")
public class RouteServiceImpl implements RouteService {

	private static final long serialVersionUID = 1L;

	@Autowired
	private RouteDAO routeDAO;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	public void insert(Route route) {
		routeDAO.insert(route);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	public void update(Route route) {
		routeDAO.update(route);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	public void delete(int idRoute) {
		routeDAO.delete(idRoute);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
	public Route getById(int id) {
		return routeDAO.getById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
	public List<Route> getAll() {
		return routeDAO.getAll();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
	public Route getWithSecondaryRoute(Integer id) {
		return routeDAO.getWithSecondaryRoute(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
	public List<Route> getResultList(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		List<Route> allRoutes = new ArrayList<Route>();
		allRoutes.addAll(routeDAO.getAll(first, pageSize, sortField, sortOrder, filters));
		for (Route route : allRoutes) {
			route.getDays().size();
		}
		return allRoutes;
	}

	@Override
	@Transactional(readOnly = true)
	public Long count(Map<String, Object> filters) {
		return routeDAO.count(filters);
	}

	@Override
	@Transactional
	public void save(Route route) {
		routeDAO.save(route);
	}

}
