package com.autogara.spring.dao;

import java.util.List;

import com.autogara.spring.model.Office;

public interface OfficeDAO extends GenericDAO<Office> {

	List<String> getAllOfficeName();
}
