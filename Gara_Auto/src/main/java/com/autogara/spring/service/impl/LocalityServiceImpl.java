package com.autogara.spring.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.LocalityDAO;
import com.autogara.spring.model.Locality;
import com.autogara.spring.service.LocalityService;

@Service("localityService")
public class LocalityServiceImpl implements LocalityService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private LocalityDAO localityDAO;

	@Override
	@Transactional(readOnly = true)
	public List<String> getAllStartLocalityName() {
		return localityDAO.getAllStartLocalityName();
	}

	@Override
	@Transactional(readOnly = true)
	public List<String> getAllDestinationLocalityName() {
		return localityDAO.getAllDestinationLocalityName();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Locality> getAll() {
		return localityDAO.getAll();
	}

	@Override
	@Transactional
	public void insert(Locality locality) {
		localityDAO.insert(locality);
	}

	@Override
	@Transactional(readOnly = true)
	public Locality getById(Integer id) {
		return localityDAO.getById(id);
	}

}
