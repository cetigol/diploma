package com.autogara.spring.service;

import java.io.Serializable;
import java.util.List;

import com.autogara.spring.model.Office;

public interface OfficeService extends Serializable {

	/**
	 * Insert the data into database(table=Office).
	 * 
	 * @param Office
	 *            The data wich we want to insert.
	 */
	public void insert(Office Office);

	/**
	 * Update the registration into database by id(table=Office).
	 * 
	 * @param Office
	 *            The data wich we want to set.
	 */
	public void update(Office Office);

	/**
	 * Delete the registration from database(table Office).
	 * 
	 * @param idOffice
	 *            The ID of the registration wich we want to delete.
	 */
	public void delete(int idOffice);

	/**
	 * Returns an object with data from the database by id(table=Office).
	 * 
	 * @param id
	 *            The ID of the registration wich we want to select.
	 * @return Return an object of the type Office.
	 */
	public Office getById(int id);

	/**
	 * Return a list of the object Office from database(table=Office)
	 * 
	 * @return Return an List.
	 */
	public List<Office> getAll();

	List<String> getAllOfficeName();
}
