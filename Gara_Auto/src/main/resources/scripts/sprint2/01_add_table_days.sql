CREATE TABLE `autogara`.`days` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

  
CREATE TABLE `autogara`.`days_routes` (
  `fk_route` INT NOT NULL,
  `fk_day` INT NOT NULL,
  INDEX `fk_route_day_idx` (`fk_route` ASC),
  INDEX `fk_day_route_idx` (`fk_day` ASC),
  CONSTRAINT `fk_route_day`
    FOREIGN KEY (`fk_route`)
    REFERENCES `autogara`.`route` (`idRoute`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_day_route`
    FOREIGN KEY (`fk_day`)
    REFERENCES `autogara`.`days` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);