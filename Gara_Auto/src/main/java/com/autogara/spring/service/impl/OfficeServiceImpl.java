package com.autogara.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.OfficeDAO;
import com.autogara.spring.model.Office;
import com.autogara.spring.service.OfficeService;

@Service("OfficeService")
public class OfficeServiceImpl implements OfficeService {

	private static final long serialVersionUID = 1L;
	@Autowired
	private OfficeDAO officeDao;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	@Override
	public void insert(Office Office) {
		officeDao.insert(Office);
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	@Override
	public void update(Office Office) {
		officeDao.update(Office);
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	@Override
	public void delete(int idOffice) {
		officeDao.delete(idOffice);
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
	@Override
	public Office getById(int id) {
		return officeDao.getById(id);
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
	@Override
	public List<Office> getAll() {
		return officeDao.getAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<String> getAllOfficeName() {
		return officeDao.getAllOfficeName();
	}
}
