package com.autogara.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.autogara.spring.model.SecondaryRoute;

@FacesConverter(value = "secondaryRouteConverter")
public class SecondaryRouteConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		SecondaryRoute secondaryRoute = new SecondaryRoute();
		if (value == null || value.isEmpty()) {
			return null;
		} else {
			Integer id = Integer.parseInt(value);
			secondaryRoute.setIdSecondaryRoute(id);
			return secondaryRoute;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SecondaryRoute secondaryRoute = (SecondaryRoute) value;
		if (secondaryRoute == null) {
			return null;
		} else {
			return secondaryRoute.getIdSecondaryRoute().toString();
		}
	}

}
