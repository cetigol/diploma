package com.autogara.spring.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "secondaryroute")
public class SecondaryRoute implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idSecondaryRoute")
	private Integer idSecondaryRoute;

	@Column(name = "description")
	private String description;

	@Column(name = "time_travel")
	private String timeTravel;

	@Column(name = "price")
	private Double price;

	@ManyToMany(mappedBy = "secondaryRoutes")
	private List<Route> routes;

	@ManyToOne
	@JoinColumn(name = "fk_locality")
	private Locality locality;

	public Integer getIdSecondaryRoute() {
		return idSecondaryRoute;
	}

	public void setIdSecondaryRoute(Integer idSecondaryRoute) {
		this.idSecondaryRoute = idSecondaryRoute;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	public Locality getLocality() {
		return locality;
	}

	public void setLocality(Locality locality) {
		this.locality = locality;
	}

	public String getTimeTravel() {
		return timeTravel;
	}

	public void setTimeTravel(String timeTravel) {
		this.timeTravel = timeTravel;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
