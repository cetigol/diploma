ALTER TABLE `autogara`.`secondaryroute` 
ADD COLUMN `fk_locality` INT NULL AFTER `description`,
ADD INDEX `fk_secondaryroute_locality_idx` (`fk_locality` ASC);
ALTER TABLE `autogara`.`secondaryroute` 
ADD CONSTRAINT `fk_secondaryroute_locality`
  FOREIGN KEY (`fk_locality`)
  REFERENCES `autogara`.`locality` (`idLocality`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
drop table autogara.routepass; 