package com.autogara.spring.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "route")
public class Route implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idRoute")
	private Integer idRoute;

	@ManyToOne
	@JoinColumn(name = "fk_startLocality")
	private Locality startLocality;

	@ManyToOne
	@JoinColumn(name = "fk_destinationLocality")
	private Locality destinationLocality;

	@Column(name = "price")
	private Double price;

	@Column(name = "timeTravel")
	private String timeTravel;

	@Column(name = "distance")
	private Integer distance;

	@Column(name = "timeStart")
	private String timeStart;

	@ManyToOne
	@JoinColumn(name = "fk_car")
	private Bus bus;

	@ManyToOne
	@JoinColumn(name = "fk_user")
	private User user;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "routecontaint", joinColumns = { @JoinColumn(name = "fkRouteSec") }, inverseJoinColumns = {
			@JoinColumn(name = "fkSecRoute") })
	private List<SecondaryRoute> secondaryRoutes;

	@ManyToOne
	@JoinColumn(name = "fk_office")
	private Office office;

	public List<DayRoute> getDays() {
		return days;
	}

	public void setDays(List<DayRoute> days) {
		this.days = days;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "days_routes", joinColumns = { @JoinColumn(name = "fk_route") }, inverseJoinColumns = {
			@JoinColumn(name = "fk_day") })
	private List<DayRoute> days;

	public Route() {

	}

	public Route(Integer id, Locality start, Locality destination, Double price, String time, String startTime,
			Integer idOffice) {
		this.idRoute = id;
		this.startLocality = start;
		this.destinationLocality = destination;
		this.price = price;
		this.timeTravel = time;
		this.timeStart = startTime;
		this.office = new Office();
		this.office.setIdOffice(idOffice);
	}

	public Integer getIdRoute() {
		return idRoute;
	}

	public void setIdRoute(Integer idRoute) {
		this.idRoute = idRoute;
	}

	public Locality getStartLocality() {
		return startLocality;
	}

	public void setStartLocality(Locality startLocality) {
		this.startLocality = startLocality;
	}

	public Locality getDestinationLocality() {
		return destinationLocality;
	}

	public void setDestinationLocality(Locality destinationLocality) {
		this.destinationLocality = destinationLocality;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getTimeTravel() {
		return timeTravel;
	}

	public void setTimeTravel(String timeTravel) {
		this.timeTravel = timeTravel;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<SecondaryRoute> getSecondaryRoutes() {
		return secondaryRoutes;
	}

	public void setSecondaryRoutes(List<SecondaryRoute> secondaryRoutes) {
		this.secondaryRoutes = secondaryRoutes;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public Bus getBus() {
		return bus;
	}

	public void setBus(Bus bus) {
		this.bus = bus;
	}

}
