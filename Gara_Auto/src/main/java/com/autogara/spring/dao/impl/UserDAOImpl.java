package com.autogara.spring.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.UserDAO;
import com.autogara.spring.model.User;

@Repository
public class UserDAOImpl extends GenericDAOImpl<User, Long> implements UserDAO, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public User getUserByUserName(String userName) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userName", userName));
		return (User) criteria.uniqueResult();
	}

}
