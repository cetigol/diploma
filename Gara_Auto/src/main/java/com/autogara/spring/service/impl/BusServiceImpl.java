package com.autogara.spring.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.BusDAO;
import com.autogara.spring.model.Bus;
import com.autogara.spring.service.BusService;

@Service("busService")
public class BusServiceImpl implements BusService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private BusDAO busDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Bus> getAll() {
		return busDAO.getAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Bus getById(Integer id) {
		return busDAO.getById(id);
	}

	@Override
	@Transactional
	public void insert(Bus bus) {
		busDAO.insert(bus);
	}

}
