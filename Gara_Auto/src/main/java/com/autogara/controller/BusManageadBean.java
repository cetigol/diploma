package com.autogara.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.autogara.spring.model.Bus;
import com.autogara.spring.model.Color;
import com.autogara.spring.service.BusService;
import com.autogara.spring.service.ColorService;

@ManagedBean(name = "busMB")
@ViewScoped
public class BusManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{busService}")
	private BusService busService;

	@ManagedProperty(value = "#{colorService}")
	private ColorService colorService;

	private Color selectedColor;
	private Bus newBus;

	private List<Color> colors;

	@PostConstruct
	public void init() {
		newBus = new Bus();
		selectedColor = new Color();

		colors = colorService.getAll();
	}

	public void saveBus() {
		newBus.setColor(selectedColor);
		busService.insert(newBus);
	}

	public BusService getBusService() {
		return busService;
	}

	public void setBusService(BusService busService) {
		this.busService = busService;
	}

	public ColorService getColorService() {
		return colorService;
	}

	public void setColorService(ColorService colorService) {
		this.colorService = colorService;
	}

	public Color getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
	}

	public Bus getNewBus() {
		return newBus;
	}

	public void setNewBus(Bus newBus) {
		this.newBus = newBus;
	}

	public List<Color> getColors() {
		return colors;
	}

	public void setColors(List<Color> colors) {
		this.colors = colors;
	}

}
