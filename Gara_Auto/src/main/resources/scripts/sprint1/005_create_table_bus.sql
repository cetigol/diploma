CREATE TABLE `autogara`.`bus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `number` VARCHAR(45) NULL,
  `color` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `autogara`.`route` 
CHANGE COLUMN `car` `fk_car` INT NULL DEFAULT NULL ,
ADD INDEX `Fk_route_car_idx` (`fk_car` ASC);
ALTER TABLE `autogara`.`route` 
ADD CONSTRAINT `Fk_route_car`
  FOREIGN KEY (`fk_car`)
  REFERENCES `autogara`.`bus` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  
CREATE TABLE `autogara`.`color` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `autogara`.`bus` 
CHANGE COLUMN `color` `fk_color` INT NULL DEFAULT NULL ,
ADD INDEX `Fk_bus_color_idx` (`fk_color` ASC);
ALTER TABLE `autogara`.`bus` 
ADD CONSTRAINT `Fk_bus_color`
  FOREIGN KEY (`fk_color`)
  REFERENCES `autogara`.`color` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
