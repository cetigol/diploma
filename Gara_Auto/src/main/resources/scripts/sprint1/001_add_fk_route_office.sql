ALTER TABLE `autogara`.`route` 
ADD COLUMN `fk_office` INT NULL AFTER `fk_user`,
ADD INDEX `Fk_route_office_idx` (`fk_office` ASC);
ALTER TABLE `autogara`.`route` 
ADD CONSTRAINT `Fk_route_office`
  FOREIGN KEY (`fk_office`)
  REFERENCES `autogara`.`office` (`idOffice`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
