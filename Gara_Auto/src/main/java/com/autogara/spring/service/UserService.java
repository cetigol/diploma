package com.autogara.spring.service;

import com.autogara.spring.model.User;

public interface UserService {

	User getUserByLogin(String userName);
}
