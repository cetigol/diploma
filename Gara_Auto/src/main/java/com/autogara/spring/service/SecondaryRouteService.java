package com.autogara.spring.service;

import java.util.List;

import com.autogara.spring.model.SecondaryRoute;

public interface SecondaryRouteService {
	List<SecondaryRoute> getAll();

	List<SecondaryRoute> getAllWithoutRoute();
	
	void saveOrUpdate(SecondaryRoute secondaryRoute);
}
