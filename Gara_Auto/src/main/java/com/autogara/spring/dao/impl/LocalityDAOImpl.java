package com.autogara.spring.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.LocalityDAO;
import com.autogara.spring.model.Locality;

@Repository
public class LocalityDAOImpl extends GenericDAOImpl<Locality, Long> implements LocalityDAO, Serializable {

	private static final long serialVersionUID = 1L;

	private static final String GET_LOCALITY_NAME_FROM_ROUTE_SQL = "select distinct l.name from route as r left join locality as l on ";

	@Override
	public List<String> getAllStartLocalityName() {
		String sqlQuery = GET_LOCALITY_NAME_FROM_ROUTE_SQL + "r.fk_startLocality = l.idLocality";
		return getListNameFromSQLQuery(sqlQuery);
	}

	@Override
	public List<String> getAllDestinationLocalityName() {
		String sqlQuery = GET_LOCALITY_NAME_FROM_ROUTE_SQL + "r.fk_destinationLocality = l.idLocality";
		sqlQuery += " union	select distinct l2.name from route as r left join routecontaint as rc "
				+ "on r.idRoute = rc.fkRouteSec left join secondaryroute as s on rc.fkSecRoute = s.idSecondaryRoute "
				+ "left join locality as l2 on l2.idLocality=s.fk_locality where name != null";
		return getListNameFromSQLQuery(sqlQuery);
	}

	@SuppressWarnings("unchecked")
	private List<String> getListNameFromSQLQuery(String sqlQuery) {
		Query query = getCurrentSession().createSQLQuery(sqlQuery);
		return query.list();
	}

}
