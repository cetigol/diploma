package com.autogara.spring.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.autogara.spring.model.Route;

public interface RouteService extends Serializable {
	/**
	 * Insert the data into database(table=Route).
	 * 
	 * @param Route
	 *            The data wich we want to insert.
	 */
	public void insert(Route route);

	/**
	 * Update the registration into database by id(table=Route).
	 * 
	 * @param Route
	 *            The data wich we want to set.
	 */
	public void update(Route route);

	/**
	 * Delete the registration from database(table Route).
	 * 
	 * @param idRoute
	 *            The ID of the registration wich we want to delete.
	 */
	public void delete(int idRoute);

	/**
	 * Returns an object with data from the database by id(table=Route).
	 * 
	 * @param id
	 *            The ID of the registration wich we want to select.
	 * @return Return an object of the type Route.
	 */
	public Route getById(int id);

	/**
	 * Return a list of the object Route from database(table=Route)
	 * 
	 * @return Return an List.
	 */
	public List<Route> getAll();

	/**
	 * Return an object Route from database(table=Route) with secondary route
	 * 
	 * @return Return an object.
	 */
	public Route getWithSecondaryRoute(Integer id);

	/**
	 * Lazy load
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 */
	public List<Route> getResultList(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters);

	public Long count(Map<String, Object> filters);

	public void save(Route route);
}
