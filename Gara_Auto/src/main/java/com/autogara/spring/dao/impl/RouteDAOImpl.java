package com.autogara.spring.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.RouteDAO;
import com.autogara.spring.model.Route;

@Repository
public class RouteDAOImpl extends GenericDAOImpl<Route, Long> implements RouteDAO, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public Route getWithSecondaryRoute(Integer id) {
		String hql = "select r from Route r left join fetch r.secondaryRoutes as sr left join fetch sr.localites as l WHERE r.id = :id";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		return (Route) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Route> getAll(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		Criteria crit = getCurrentSession().createCriteria(Route.class);
		crit.createAlias("startLocality", "startLocality");
		crit.createAlias("destinationLocality", "destinationLocality");
		crit.createAlias("office", "office", JoinType.LEFT_OUTER_JOIN);
		crit.createAlias("bus", "bus", JoinType.LEFT_OUTER_JOIN);

		if (sortField != null && !sortField.isEmpty()) {
			if (sortOrder != null) {
				if (sortOrder == SortOrder.ASCENDING) {
					crit.addOrder(Order.asc(sortField));
				} else {
					crit.addOrder(Order.desc(sortField));
				}
			}
		}

		if (filters != null && !filters.isEmpty()) {
			Iterator<Entry<String, Object>> iterator = filters.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> entry = iterator.next();
				Criterion rest1 = Restrictions.eq(entry.getKey(), entry.getValue().toString());
				if (entry.getKey().equals("destinationLocality.name")) {
					crit.createAlias("secondaryRoutes", "secondaryRoute", JoinType.LEFT_OUTER_JOIN);
					crit.createAlias("secondaryRoute.locality", "secLocality", JoinType.LEFT_OUTER_JOIN);
					Criterion rest2 = Restrictions.eq("secLocality.name", entry.getValue().toString());
					crit.add(Restrictions.or(rest1, rest2));
				} else {
					crit.add(rest1);
				}
			}
		}

		crit.setFirstResult(first).setMaxResults(pageSize);

		List<Route> result = crit.list();
		return result;
	}

	@Override
	public Long count(Map<String, Object> filters) {
		Criteria criteria = getCurrentSession().createCriteria(Route.class);
		criteria.createAlias("startLocality", "startLocality");
		criteria.createAlias("destinationLocality", "destinationLocality");
		criteria.createAlias("office", "office", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("bus", "bus", JoinType.LEFT_OUTER_JOIN);

		if (filters != null && !filters.isEmpty()) {
			Iterator<Entry<String, Object>> iterator = filters.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> entry = iterator.next();
				Criterion rest1 = Restrictions.eq(entry.getKey(), entry.getValue().toString());
				if (entry.getKey().equals("destinationLocality.name")) {
					criteria.createAlias("secondaryRoutes", "secondaryRoute", JoinType.LEFT_OUTER_JOIN);
					criteria.createAlias("secondaryRoute.locality", "secLocality", JoinType.LEFT_OUTER_JOIN);
					Criterion rest2 = Restrictions.eq("secLocality.name", entry.getValue().toString());
					criteria.add(Restrictions.or(rest1, rest2));
				} else {
					criteria.add(rest1);
				}
			}
		}

		return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
	}

}
