package com.autogara.spring.dao;

import com.autogara.spring.model.User;

public interface UserDAO extends GenericDAO<User> {

	User getUserByUserName(String userName);

}
