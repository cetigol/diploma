package com.autogara.spring.util;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;

public class WriteToProperties {
	public WriteToProperties() {

	}

	public boolean write(String nameEN, String nameRO, String nameRU, String name) {
		nameEN = escapeNonAscii(nameEN);
		nameRO = escapeNonAscii(nameRO);
		nameRU = escapeNonAscii(nameRU);
		// try {
		// Files.write(Paths.get("D:/git_repository/Gara_Auto/src/main/resources/messages/messages_en.properties"),
		// ("\n" + name + "=" + nameEN).getBytes(), StandardOpenOption.APPEND);
		// Files.write(Paths.get("D:/git_repository/Gara_Auto/src/main/resources/messages/messages_ro.properties"),
		// ("\n" + name + "=" + nameRO).getBytes(), StandardOpenOption.APPEND);
		// Files.write(Paths.get("D:/git_repository/Gara_Auto/src/main/resources/messages/messages_ru.properties"),
		// ("\n" + name + "=" + nameRU).getBytes(), StandardOpenOption.APPEND);
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// }

		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream(
					getClass().getClassLoader().getResource("messages/messages_en.properties").getFile(), true);

			// set the properties value
			Writer out = new BufferedWriter(new OutputStreamWriter(output, "ISO-8859-1"));
			// set the properties value
			out.append("\r\n");
			out.append(name + "=" + nameRU);
			out.flush();
			out.close();

			output = new FileOutputStream(
					getClass().getClassLoader().getResource("messages/messages_ro.properties").getFile(), true);
			out = new BufferedWriter(new OutputStreamWriter(output, "ISO-8859-1"));
			// set the properties value
			out.append("\r\n");
			out.append(name + "=" + nameRO);
			out.flush();
			out.close();

			output = new FileOutputStream(
					getClass().getClassLoader().getResource("messages/messages_ru.properties").getFile(), true);
			// set the properties value
			out = new BufferedWriter(new OutputStreamWriter(output, "ISO-8859-1"));
			// set the properties value
			out.append("\r\n");
			out.append(name + "=" + nameRO);
			out.flush();
			out.close();

			return true;
		} catch (IOException io) {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			io.printStackTrace();
		}
		return false;
	}

	private String escapeNonAscii(String str) {

		StringBuilder retStr = new StringBuilder();
		for (int i = 0; i < str.length(); i++) {
			int cp = Character.codePointAt(str, i);
			int charCount = Character.charCount(cp);
			if (charCount > 1) {
				i += charCount - 1; // 2.
				if (i >= str.length()) {
					throw new IllegalArgumentException("truncated unexpectedly");
				}
			}

			if (cp < 128) {
				retStr.appendCodePoint(cp);
			} else {
				retStr.append(String.format("\\u0%x", cp));
			}
		}
		return retStr.toString();
	}

}
