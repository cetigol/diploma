package com.autogara.spring.dao;

import java.util.List;

import com.autogara.spring.model.SecondaryRoute;

public interface SecondaryRouteDAO extends GenericDAO<SecondaryRoute> {
	
	List<SecondaryRoute> getAllWithoutRoute();
}
