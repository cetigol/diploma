package com.autogara.spring.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.LocalityDAO;
import com.autogara.spring.dao.SecondaryRouteDAO;
import com.autogara.spring.model.SecondaryRoute;

@Repository
public class SecondaryRouteDAOImpl extends GenericDAOImpl<SecondaryRoute, Long>
		implements SecondaryRouteDAO, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private LocalityDAO localityDAO;

	@SuppressWarnings("unchecked")
	@Override
	public List<SecondaryRoute> getAllWithoutRoute() {
		String sql = "select * from secondaryroute where idSecondaryRoute not in(select fkSecRoute from routecontaint)";
        List<Map<String, Object>> results = getCurrentSession().createSQLQuery(sql).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();
		List<SecondaryRoute> routes = new ArrayList<SecondaryRoute>();
        for (Map<String, Object> map : results) {
        	routes.add(mapSecondaryRouteRow(map));
        }
        return routes;
	}
	
	private SecondaryRoute mapSecondaryRouteRow(Map<String, Object> map){
		SecondaryRoute secondaryRoute= new SecondaryRoute();
		secondaryRoute.setIdSecondaryRoute((Integer)map.get("idSecondaryRoute"));
		secondaryRoute.setDescription((String)map.get("description"));
		secondaryRoute.setTimeTravel((String)map.get("time_travel"));
		secondaryRoute.setPrice(Double.parseDouble(((Float)map.get("price")).toString()));
		secondaryRoute.setLocality(localityDAO.getById((Integer)map.get("fk_locality")));
		return secondaryRoute;
	}

}
