package com.autogara.spring.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.UserDAO;
import com.autogara.spring.model.Role;
import com.autogara.spring.model.User;
import com.autogara.spring.model.enums.UserStatus;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired(required = true)
	private UserDAO userDAO;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userDAO.getUserByUserName(userName);
		if (user != null) {
			String password = user.getPassword();
			// additional information
			boolean enabled = user.getStatus().equals(UserStatus.ACTIVE);
			boolean accountNonExpired = user.getStatus().equals(UserStatus.ACTIVE);
			boolean credentialsNonExpired = user.getStatus().equals(UserStatus.ACTIVE);
			boolean accountNonLocked = user.getStatus().equals(UserStatus.ACTIVE);

			Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
			for (Role role : user.getRoles()) {
				authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
			}

			org.springframework.security.core.userdetails.User securityUser = new org.springframework.security.core.userdetails.User(
					userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,
					authorities);
			return securityUser;
		} else {
			throw new UsernameNotFoundException("User not found");
		}

	}

}
