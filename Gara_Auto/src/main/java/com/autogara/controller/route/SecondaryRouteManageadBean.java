package com.autogara.controller.route;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.autogara.spring.model.Locality;
import com.autogara.spring.model.SecondaryRoute;
import com.autogara.spring.service.LocalityService;
import com.autogara.spring.service.SecondaryRouteService;

@ManagedBean(name = "secondaryRouteMB")
@ViewScoped
public class SecondaryRouteManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SecondaryRoute secondaryRoute;
	private Locality locality;
	private String timeHour;
	private String timeMinutes;

	private List<Locality> localitys;

	@ManagedProperty(value = "#{localityService}")
	private LocalityService localityService;
	@ManagedProperty(value = "#{secondaryRouteService}")
	private SecondaryRouteService secondaryRouteService;

	@PostConstruct
	public void init() {
		secondaryRoute = new SecondaryRoute();
		locality = new Locality();
		localitys = localityService.getAll();
		timeHour = "";
		timeMinutes = "";
	}

	public void save(){
		secondaryRoute.setLocality(locality);
		secondaryRoute.setTimeTravel(timeHour+":"+timeMinutes);
		secondaryRouteService.saveOrUpdate(secondaryRoute);
	}
	
	public SecondaryRoute getSecondaryRoute() {
		return secondaryRoute;
	}

	public void setSecondaryRoute(SecondaryRoute secondaryRoute) {
		this.secondaryRoute = secondaryRoute;
	}

	public List<Locality> getLocalitys() {
		return localitys;
	}

	public void setLocalitys(List<Locality> localitys) {
		this.localitys = localitys;
	}

	public Locality getLocality() {
		return locality;
	}

	public void setLocality(Locality locality) {
		this.locality = locality;
	}

	public String getTimeHour() {
		return timeHour;
	}

	public void setTimeHour(String timeHour) {
		this.timeHour = timeHour;
	}

	public String getTimeMinutes() {
		return timeMinutes;
	}

	public void setTimeMinutes(String timeMinutes) {
		this.timeMinutes = timeMinutes;
	}

	public LocalityService getLocalityService() {
		return localityService;
	}

	public void setLocalityService(LocalityService localityService) {
		this.localityService = localityService;
	}

	public SecondaryRouteService getSecondaryRouteService() {
		return secondaryRouteService;
	}

	public void setSecondaryRouteService(SecondaryRouteService secondaryRouteService) {
		this.secondaryRouteService = secondaryRouteService;
	}

}
