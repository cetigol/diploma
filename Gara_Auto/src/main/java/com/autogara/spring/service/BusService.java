package com.autogara.spring.service;

import java.util.List;

import com.autogara.spring.model.Bus;

public interface BusService {

	List<Bus> getAll();

	Bus getById(Integer id);

	void insert(Bus bus);
}
