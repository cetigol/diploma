package com.autogara.spring.dao;

import com.autogara.spring.model.Color;

public interface ColorDAO extends GenericDAO<Color> {

}
