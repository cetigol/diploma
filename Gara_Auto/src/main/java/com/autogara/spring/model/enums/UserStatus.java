package com.autogara.spring.model.enums;

public enum UserStatus {

	ACTIVE, INACTIVE
}
