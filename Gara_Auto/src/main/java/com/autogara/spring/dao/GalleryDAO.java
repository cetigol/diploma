package com.autogara.spring.dao;

import com.autogara.spring.model.Gallery;

public interface GalleryDAO extends GenericDAO<Gallery> {

}
