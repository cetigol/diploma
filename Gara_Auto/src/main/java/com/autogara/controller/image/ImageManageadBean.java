package com.autogara.controller.image;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.autogara.spring.model.Gallery;
import com.autogara.spring.service.GalleryService;
import com.autogara.spring.service.UserService;

@ManagedBean(name = "imageMB")
@ViewScoped
public class ImageManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> images;
	// configuram in server.xml context path
//	private static final String PATH_UPLOAD = "C:/images";
	private static final String PATH_UPLOAD = "/var/lib/openshift/575324b97628e1198d0000b4/app-root/data/images";
	private UploadedFile file;

	private Gallery newImage;

	@ManagedProperty(value = "#{galleryService}")
	private GalleryService galleryService;

	@ManagedProperty(value = "#{userService}")
	private UserService userService;

	private String fileName;

	private List<Gallery> galleryImages;

	@PostConstruct
	public void init() {
		images = new ArrayList<String>();
		newImage = new Gallery();
		galleryImages = galleryService.getAll();
		for (Gallery gallery : galleryImages) {
			images.add(gallery.getImageName());
		}
	}

	public void upload(FileUploadEvent event) {
		file = event.getFile();
		fileName = file.getFileName();
	}

	public void save() {
		if (file != null) {
			try {
				InputStream input = file.getInputstream();
				// Path nFile = Files.createTempFile(folder, filename, "." +
				// extension);

				File f = new File(PATH_UPLOAD + "/" + file.getFileName());
				if (f.createNewFile()) {
					Files.copy(input, f.toPath(), StandardCopyOption.REPLACE_EXISTING);

					newImage.setImageName(file.getFileName());
					Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
					String currentPrincipalName = authentication.getName();

					newImage.setUploadedBy(userService.getUserByLogin(currentPrincipalName));

					galleryService.insert(newImage);
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}
	}

	public List<String> getImages() {
		return images;
	}

	public GalleryService getGalleryService() {
		return galleryService;
	}

	public void setGalleryService(GalleryService galleryService) {
		this.galleryService = galleryService;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Gallery getNewImage() {
		return newImage;
	}

	public void setNewImage(Gallery newImage) {
		this.newImage = newImage;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Gallery> getGalleryImages() {
		return galleryImages;
	}

	public void setGalleryImages(List<Gallery> galleryImages) {
		this.galleryImages = galleryImages;
	}

}
