package com.autogara.controller.language;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

@ManagedBean(name = "language", eager = true)
@SessionScoped
public class LanguageBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Locale locale;

	@PostConstruct
	public void init() {
		locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}

	public Locale getLocale() {
		return locale;
	}

	public String getLanguage() {
		return locale.getLanguage();
	}

	public void changeLanguage(String language, String country) {
		if (language.equalsIgnoreCase("ru")) {
			locale = new Locale.Builder().setLanguage(language).setRegion("RU").setScript("Cyrl").setExtension('u', language).build();
		} else {
			locale = new Locale.Builder().setLanguage(language).setScript("Latn").build();
		}
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();

		if (viewId.equals("/index.xhtml")) {
			RequestContext.getCurrentInstance().execute(
					"$(window).bind('load', function(){document.getElementById('base:mapForm:resetButton').click();});");
		}
	}
}