package com.autogara.controller.security;

import java.util.Collection;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

@ManagedBean(name = "security")
@RequestScoped
public class SecurityTagBean {

	public boolean isAuthenticated;

	@SuppressWarnings("unchecked")
	public boolean hasRole(String role) {
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		for (SimpleGrantedAuthority simpleGrantedAuthority : authorities) {
			if (simpleGrantedAuthority.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	public boolean isAuthenticated() {
		if (!SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")) {
			setAuthenticated(SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
		} else {
			setAuthenticated(false);
		}
		return isAuthenticated;
	}

	public void setAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	public String logout() {
		return "logout?faces-redirect=true";
	}

}
