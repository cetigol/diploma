package com.autogara.spring.service;

import java.util.List;

import com.autogara.spring.model.Locality;

public interface LocalityService {

	List<String> getAllStartLocalityName();

	List<String> getAllDestinationLocalityName();

	List<Locality> getAll();

	void insert(Locality locality);

	Locality getById(Integer id);
}
