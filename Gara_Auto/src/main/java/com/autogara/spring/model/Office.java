package com.autogara.spring.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "office")
public class Office implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idOffice")
	private Integer idOffice;

	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "phone")
	private String phone;

	@ManyToOne
	@JoinColumn(name = "fkLocality")
	private Locality locality;

	@OneToMany(mappedBy = "office")
	private List<User> users;

	public Office() {

	}

	public Office(Integer id, String name, String address, String locality, String lat, String longi) {
		this.idOffice = id;
		this.name = name;
		this.address = address;
		this.locality = new Locality();
		this.locality.setName(locality);
		this.locality.setLatitude(lat);
		this.locality.setLongitude(longi);
		this.phone = "phone";
	}

	public Integer getIdOffice() {
		return idOffice;
	}

	public void setIdOffice(Integer idOffice) {
		this.idOffice = idOffice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Locality getLocality() {
		return locality;
	}

	public void setLocality(Locality locality) {
		this.locality = locality;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}
