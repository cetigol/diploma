package com.autogara.spring.dao;

import com.autogara.spring.model.Bus;

public interface BusDAO extends GenericDAO<Bus> {

}
