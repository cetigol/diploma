package com.autogara.controller.office;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.autogara.spring.model.Locality;
import com.autogara.spring.model.Office;
import com.autogara.spring.service.LocalityService;
import com.autogara.spring.service.OfficeService;
import com.autogara.spring.util.WriteToProperties;

@ManagedBean(name = "officeMB")
@ViewScoped
public class OfficeManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String OFFICE_PREFIX = "bus.stations.";

	@ManagedProperty(value = "#{OfficeService}")
	private OfficeService officeService;

	@ManagedProperty(value = "#{localityService}")
	private LocalityService localityService;

	private String nameRO;
	private String nameEN;
	private String nameRU;
	private Office newOffice;
	private Locality selectedLocality;

	private List<Locality> localities;

	@PostConstruct
	public void init() {
		newOffice = new Office();
		selectedLocality = new Locality();
		localities = localityService.getAll();
	}

	public void save() {
		newOffice.setLocality(selectedLocality);
		String name = OFFICE_PREFIX + nameEN.toLowerCase().replaceAll("\u0219", "s").replaceAll("\u021B", "t")
				.replaceAll("\u0103", "a").replaceAll("\u0203", "a").replaceAll("\u020B", "i");
		newOffice.setName(name);
		WriteToProperties util = new WriteToProperties();
		if (util.write(nameEN, nameRO, nameRU, name)) {
			officeService.insert(newOffice);
			newOffice = new Office();
			nameRO = "";
			nameEN = "";
			nameRU = "";
		}
	}

	public OfficeService getOfficeService() {
		return officeService;
	}

	public void setOfficeService(OfficeService officeService) {
		this.officeService = officeService;
	}

	public LocalityService getLocalityService() {
		return localityService;
	}

	public void setLocalityService(LocalityService localityService) {
		this.localityService = localityService;
	}

	public String getNameRO() {
		return nameRO;
	}

	public void setNameRO(String nameRO) {
		this.nameRO = nameRO;
	}

	public String getNameEN() {
		return nameEN;
	}

	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}

	public String getNameRU() {
		return nameRU;
	}

	public void setNameRU(String nameRU) {
		this.nameRU = nameRU;
	}

	public Office getNewOffice() {
		return newOffice;
	}

	public void setNewOffice(Office newOffice) {
		this.newOffice = newOffice;
	}

	public Locality getSelectedLocality() {
		return selectedLocality;
	}

	public void setSelectedLocality(Locality selectedLocality) {
		this.selectedLocality = selectedLocality;
	}

	public List<Locality> getLocalities() {
		return localities;
	}

	public void setLocalities(List<Locality> localities) {
		this.localities = localities;
	}

}
