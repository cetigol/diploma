package com.autogara.spring.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.autogara.spring.model.Route;
import com.autogara.spring.service.RouteService;

public class LazyRouteDataModel extends LazyDataModel<Route> {

	private static final long serialVersionUID = 1L;

	private RouteService crudService;

	public LazyRouteDataModel() {

	}

	public LazyRouteDataModel(RouteService routeService) {
		this.crudService = routeService;
	}

	@Override
	public List<Route> load(int first, int pageSize, String sortField, final SortOrder sortOrder,
			Map<String, Object> filters) {
		setRowCount(crudService.count(filters).intValue());
		List<Route> routes = crudService.getResultList(first, pageSize, sortField, sortOrder, filters);
		if (sortField != null) {
			Collections.sort(routes, new Comparator<Route>() {
				@Override
				public int compare(Route route1, Route route2) {
					String[] hour1;
					String[] hour2;
					if (sortOrder == SortOrder.ASCENDING) {
						hour1 = route1.getTimeStart().split(":");
						hour2 = route2.getTimeStart().split(":");
					} else {
						hour2 = route1.getTimeStart().split(":");
						hour1 = route2.getTimeStart().split(":");
					}
					if (hour1[0] == hour2[0]) {
						if (hour1[1] == hour2[1]) {
							return 0;
						} else if (Integer.parseInt(hour1[1]) < Integer.parseInt(hour2[1])) {
							return -1;
						} else {
							return 1;
						}

					} else {
						if (hour1[0] == hour2[0]) {
							return 0;
						} else if (Integer.parseInt(hour1[0]) < Integer.parseInt(hour2[0])) {
							return -1;
						} else {
							return 1;
						}
					}
				}
			});
		}
		return routes;
	}

	@Override
	public Object getRowKey(Route object) {
		return object.getIdRoute();
	}

	@Override
	public Route getRowData(String rowKey) {
		return crudService.getById(Integer.parseInt(rowKey));
	}

	@Override
	public void setRowIndex(int rowIndex) {
		if (rowIndex == -1 || getPageSize() == 0) {
			super.setRowIndex(-1);
		} else
			super.setRowIndex(rowIndex % getPageSize());
	}
}
