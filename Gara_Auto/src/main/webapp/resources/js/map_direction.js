var global_markers = [];
var directionsDisplay = new google.maps.DirectionsRenderer({
    suppressMarkers: true
});
function calculateAndDisplayRoute(markers, gMap) {
 clearMap();

  var directionsService = new google.maps.DirectionsService;
  
// for dynamic pop up window
//  https://developers.google.com/maps/documentation/javascript/examples/directions-complex?hl=ru
  var infowindow = new google.maps.InfoWindow;
  var map = PF(gMap+'').getMap();
  directionsDisplay.setMap(map);
  directionsService.route({
	origin: new google.maps.LatLng(markers[0].latlng.lat, markers[0].latlng.lng),  
    destination: new google.maps.LatLng(markers[1].latlng.lat, markers[1].latlng.lng),
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      
      if(gMap === "gMapForm"){
    	  var length = response.routes[0].legs[0].distance.text.length;
    	  var km = response.routes[0].legs[0].distance.text.substring(0, length-3);
    	  $('*[id*=distance]:visible').val(Math.round(parseFloat(km)));
      }
      
//trebuie de analizat    http://stackoverflow.com/questions/29361044/setting-a-info-window-for-google-map-markers-located-on-route-start-and-end-poi  
      for (i = 0; i < markers.length; i++) {
        	createMarker(markers[i].latlng, markers[i].title, markers[i].data, markers[i].icon);
      }
      
      function createMarker(latlng, label, html, url) {
    	    var contentString = '<b>' + label + '</b><br>' + html;
    	    var marker = new google.maps.Marker({
    	      position: latlng,
    	      map: map,
    	      icon: url,
    	      title: label,
    	      zIndex: Math.round(latlng.lat * -100000) << 5
    	    });
    	    global_markers.push(marker);
    	    google.maps.event.addListener(marker, 'click', function() {
    	      infowindow.setContent(contentString);
    	      infowindow.open(map, marker);
    	    });
      
      }
      
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function showInfoWindow(label){
	for(var i=0; i < global_markers.length; i++){
		if(global_markers[i].title === label){
			console.log(global_markers[i]);
			google.maps.event.trigger(global_markers[i], 'click');
			break;
		}
	}
}

function clearMap() {
  for (var i = 0; i < global_markers.length; i++) {
	  global_markers[i].setMap(null);
  }
  global_markers = [];
  directionsDisplay.setMap(null);
}