package com.autogara.spring.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.autogara.spring.dao.GenericDAO;

public class GenericDAOImpl<T, E extends Serializable> implements GenericDAO<T> {

	@Autowired
	protected SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Class<T> type;

	protected Class<T> getType() {
		return this.type;
	}

	protected String getClassName() {
		return type.getName();
	}

	@SuppressWarnings("unchecked")
	public GenericDAOImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<T>) pt.getActualTypeArguments()[0];
	}

	public void delete(Integer id) {
		Object obj = getById(id);
		if (obj != null)
			getCurrentSession().delete(obj);
	}

	@SuppressWarnings("unchecked")
	public T getById(Integer id) {
		return (T) getCurrentSession().get(type, id);
	}

	public void insert(T t) {
		getCurrentSession().save(t);
	}

	public void update(T t) {
		getCurrentSession().update(t);
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return getCurrentSession().createQuery("from " + type.getName()).list();
	}

	public void save(T t) {
		getCurrentSession().saveOrUpdate(t);
	}

}
