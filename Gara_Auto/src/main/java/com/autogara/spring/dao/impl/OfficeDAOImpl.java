package com.autogara.spring.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.OfficeDAO;
import com.autogara.spring.model.Office;

@Repository
public class OfficeDAOImpl extends GenericDAOImpl<Office, Long> implements OfficeDAO, Serializable {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllOfficeName() {
		String hql = "SELECT DISTINCT o.name FROM Office o";
		Query query = getCurrentSession().createQuery(hql);
		return query.list();
	}

}
