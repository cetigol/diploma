package com.autogara.spring.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.ColorDAO;
import com.autogara.spring.model.Color;
import com.autogara.spring.service.ColorService;

@Service("colorService")
public class ColorServiceImpl implements ColorService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ColorDAO colorDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Color> getAll() {
		return colorDAO.getAll();
	}

}
