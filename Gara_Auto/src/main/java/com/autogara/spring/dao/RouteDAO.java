package com.autogara.spring.dao;

import java.util.Collection;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.autogara.spring.model.Route;

public interface RouteDAO extends GenericDAO<Route> {

	public Route getWithSecondaryRoute(Integer id);

	public Collection<Route> getAll(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters);

	public Long count(Map<String, Object> filters);
}
