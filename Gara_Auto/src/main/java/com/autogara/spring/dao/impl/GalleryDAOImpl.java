package com.autogara.spring.dao.impl;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.GalleryDAO;
import com.autogara.spring.model.Gallery;

@Repository
public class GalleryDAOImpl extends GenericDAOImpl<Gallery, Integer> implements GalleryDAO, Serializable {

	private static final long serialVersionUID = 1L;

}
