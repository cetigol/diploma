CREATE TABLE `autogara`.`gallery` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_image` VARCHAR(100) NULL,
  `fk_user` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `Fk_imgae_user_idx` (`fk_user` ASC),
  CONSTRAINT `Fk_imgae_user`
    FOREIGN KEY (`fk_user`)
    REFERENCES `autogara`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

    
ALTER TABLE `autogara`.`gallery` 
ADD COLUMN `description` VARCHAR(200) NULL AFTER `fk_user`;
