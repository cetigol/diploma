package com.autogara.spring.model.enums;

public enum LocalityStatus {

	START_LOCALITY, DESTINATION_LOCALITY, WAY_POINT
}
