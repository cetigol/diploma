package com.autogara.spring.dao.impl;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.autogara.spring.dao.ColorDAO;
import com.autogara.spring.dao.GenericDAO;
import com.autogara.spring.model.Color;

@Repository
public class ColorDAOImpl extends GenericDAOImpl<Color, Integer> implements GenericDAO<Color>, ColorDAO, Serializable {

	private static final long serialVersionUID = 1L;

}
