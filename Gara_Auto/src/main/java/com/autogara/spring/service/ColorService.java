package com.autogara.spring.service;

import java.util.List;

import com.autogara.spring.model.Color;

public interface ColorService {

	List<Color> getAll();
}
