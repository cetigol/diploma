package com.autogara.spring.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "locality")
public class Locality implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idLocality")
	private Integer idLocality;

	@Column(name = "name")
	private String name;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "latitude")
	private String latitude;

	@OneToMany(mappedBy = "locality")
	private List<Office> offices;

	@OneToMany(mappedBy = "startLocality")
	private List<Route> startRoutes;

	@OneToMany(mappedBy = "destinationLocality")
	private List<Route> destinationRoutes;

	@OneToMany(mappedBy = "locality")
	private List<SecondaryRoute> secondaryRoutes;

	public Locality() {

	}

	public Locality(Integer id, String name, String lat, String longi) {
		this.idLocality = id;
		this.name = name;
		this.latitude = lat;
		this.longitude = longi;
	}

	public Integer getIdLocality() {
		return idLocality;
	}

	public void setIdLocality(Integer idLocality) {
		this.idLocality = idLocality;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public List<Office> getOffices() {
		return offices;
	}

	public void setOffices(List<Office> offices) {
		this.offices = offices;
	}

	public List<Route> getStartRoutes() {
		return startRoutes;
	}

	public void setStartRoutes(List<Route> startRoutes) {
		this.startRoutes = startRoutes;
	}

	public List<Route> getDestinationRoutes() {
		return destinationRoutes;
	}

	public void setDestinationRoutes(List<Route> destinationRoutes) {
		this.destinationRoutes = destinationRoutes;
	}

	public List<SecondaryRoute> getSecondaryRoutes() {
		return secondaryRoutes;
	}

	public void setSecondaryRoutes(List<SecondaryRoute> secondaryRoutes) {
		this.secondaryRoutes = secondaryRoutes;
	}

}
