package com.autogara.spring.dao;

import java.util.List;

import com.autogara.spring.model.Locality;

public interface LocalityDAO extends GenericDAO<Locality> {

	List<String> getAllStartLocalityName();

	List<String> getAllDestinationLocalityName();
}
