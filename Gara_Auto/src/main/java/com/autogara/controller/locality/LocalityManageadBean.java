package com.autogara.controller.locality;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.autogara.spring.model.Locality;
import com.autogara.spring.model.Office;
import com.autogara.spring.service.LocalityService;
import com.autogara.spring.service.OfficeService;
import com.autogara.spring.util.WriteToProperties;

@ManagedBean(name = "localityMB")
@ViewScoped
public class LocalityManageadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String LOCALITY_PREFIX = "locality.";

	@ManagedProperty(value = "#{OfficeService}")
	private OfficeService officeService;

	@ManagedProperty(value = "#{localityService}")
	private LocalityService localityService;

	private List<Office> offices;

	private Locality newLocality;
	private Office selectedOffice;
	private String nameRO;
	private String nameEN;
	private String nameRU;
	private Double longitude;
	private Double latitude;

	@PostConstruct
	public void init() {
		offices = officeService.getAll();
		newLocality = new Locality();
		selectedOffice = new Office();
	}

	public void save() {
		if (selectedOffice.getIdOffice() != null) {
			newLocality.setOffices(new ArrayList<Office>());
			newLocality.getOffices().add(selectedOffice);
		}
		String name = LOCALITY_PREFIX + nameEN.toLowerCase();
		newLocality.setName(name);
		newLocality.setLongitude(longitude.toString());
		newLocality.setLatitude(latitude.toString());

		WriteToProperties util = new WriteToProperties();
		if (util.write(nameEN, nameRO, nameRU, name)) {
			localityService.insert(newLocality);
			newLocality = new Locality();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Added", "Info"));
		}

	}

	public OfficeService getOfficeService() {
		return officeService;
	}

	public void setOfficeService(OfficeService officeService) {
		this.officeService = officeService;
	}

	public LocalityService getLocalityService() {
		return localityService;
	}

	public void setLocalityService(LocalityService localityService) {
		this.localityService = localityService;
	}

	public List<Office> getOffices() {
		return offices;
	}

	public void setOffices(List<Office> offices) {
		this.offices = offices;
	}

	public Locality getNewLocality() {
		return newLocality;
	}

	public void setNewLocality(Locality newLocality) {
		this.newLocality = newLocality;
	}

	public Office getSelectedOffice() {
		return selectedOffice;
	}

	public void setSelectedOffice(Office selectedOffice) {
		this.selectedOffice = selectedOffice;
	}

	public String getNameRO() {
		return nameRO;
	}

	public void setNameRO(String nameRO) {
		this.nameRO = nameRO;
	}

	public String getNameEN() {
		return nameEN;
	}

	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}

	public String getNameRU() {
		return nameRU;
	}

	public void setNameRU(String nameRU) {
		this.nameRU = nameRU;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

}
