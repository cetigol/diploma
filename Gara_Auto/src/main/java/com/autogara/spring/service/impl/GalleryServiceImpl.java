package com.autogara.spring.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autogara.spring.dao.GalleryDAO;
import com.autogara.spring.model.Gallery;
import com.autogara.spring.service.GalleryService;

@Service("galleryService")
public class GalleryServiceImpl implements GalleryService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private GalleryDAO galleryDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Gallery> getAll() {
		return galleryDAO.getAll();
	}

	@Override
	@Transactional
	public void insert(Gallery gallery) {
		galleryDAO.insert(gallery);
	}

}
